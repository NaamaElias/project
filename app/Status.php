<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
   
    public function tasks()
    {
        return $this->hasMany('App\Task');
    } 
   
    public static function switch($status_id){
        $levels = DB::table('levels')->where('from',$status_id)->pluck('to');
        return self::find($levels)->all(); 
    }

    public static function allowed($from,$to){
        $allowed = DB::table('levels')->where('from',$from)->where('to',$to)->get();
        if(isset($allowed)) return true;
        return false;
    }
   
    
}
