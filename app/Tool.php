<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    protected $fillable = [
        'name', 'notes','user_id','status_id'
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function toolstatus(){
        return $this->belongsTo('App\Toolstatus','status_id');
    }
}
