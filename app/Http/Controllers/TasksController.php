<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;




class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function changeStatus($tid, $sid)
    {
        $task = Task::findOrFail($tid);
        $from = $task->status->id;
        if(!Status::allowed($from,$sid)) return redirect('tasks');        
        $task->status_id = $sid;
        $task->save();
        return back();
    } 

    public function myTasks()
    {        
        $tasks = Task::join('task_user','tasks.id','=','task_user.task_id')->where([['status_id','1'],['user_id',Auth::id()],])->orWhere([['status_id','3'],['user_id',Auth::id()],])->paginate('5');
        $users = User::all();
        $statuses = Status::all();        
        return view('tasks.index', compact('tasks','users', 'statuses'));
    }

    public function completedTasks()
    {     
        $tasks= Task::where('status_id','4')->paginate('5');
        $statuses = Status::all();   
        $users = User::all();  
        return view('tasks.index', compact('tasks','users','statuses'));
    }

    public function deleteAll(Request $request){
        
        Gate::authorize('is-manager');     
        $ids= $request->id;
        if($ids==null) return back();
        $max = sizeof($ids);
        for($i=0; $i<$max; $i++){
            $z=$ids[$i];        
            Task::where('id',$z)->delete();                
        }  
        return redirect('tasks');
    }

    // default functions
    public function index()
    {
        $tasks = Task::where('status_id','1')->orWhere('status_id','3')->paginate('5');
        $users = User::all();
        $statuses=Status::all();
        return view('tasks.index', compact('tasks','users','statuses')); 
    }
   
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('is-manager');
        $users = User::all();
        return view('tasks.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if($request->task_description == NULL || $request->start_date == NULL || $request->estimated_end_date == NULL || $request->user_id == NULL){
            Session::flash('NotFullForm', 'Please fill out the entire form, all fields are mandatory');
            return back();
        }
        $userId = $request->user_id;
        $newStartDate = $request->start_date;
        $newEndDate = $request->estimated_end_date;
        // allowable dates
        if($newStartDate > $newEndDate){
            Session::flash('wrongDates', 'Start date is greater than end date');
            return back();
        }
        // allowable new task
        foreach($userId as $user){
            $results[] = Task::join('task_user','tasks.id','=','task_user.task_id')
                     ->where([['status_id','<>','2'],['start_date','>=',$newStartDate],['start_date','<=',$newEndDate],['user_id',$user],])
                     ->orWhere([['status_id','<>','2'],['start_date','<=',$newStartDate],['estimated_end_date','>=',$newStartDate],['user_id',$user],])->pluck('user_id');
        }
        $max = sizeof($results);
        for($i=0; $i<$max; $i++){
            $z = $results[$i];
            if(sizeof($z) > 0){
                Session::flash('notAvailableTask', 'Overlap - One or more users are already scheduled for a task on these dates, Please change dates or users');
                return back();
            }
        }
        // create new task
        $task = new Task();
        $task=$task->create($request->all());
        $task->users()->attach($userId); 
        $task->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $task = Task::findOrFail($id);
         $users = User::all();
         return view('tasks.edit', compact('task','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if($request->task_description == NULL || $request->start_date == NULL || $request->estimated_end_date == NULL || $request->user_id == NULL){
            Session::flash('NotFullForm', 'Please fill out the entire form, all fields are mandatory');
            return back();
        }
        $task = Task::findOrFail($id);
        $task->users()->detach();

        $userId = $request->user_id;
        $newStartDate = $request->start_date;
        $newEndDate = $request->estimated_end_date;
        // allowable dates
        if($newStartDate > $newEndDate){
            Session::flash('wrongDates', 'Start date is greater than end date');
            return back();
        }
        // allowable new task
        foreach($userId as $user){
            $results[] = Task::join('task_user','tasks.id','=','task_user.task_id')
                     ->where([['status_id','<>','2'],['start_date','>=',$newStartDate],['start_date','<=',$newEndDate],['user_id',$user],])
                     ->orWhere([['status_id','<>','2'],['start_date','<=',$newStartDate],['estimated_end_date','>=',$newStartDate],['user_id',$user],])->pluck('user_id');
        }
        $max = sizeof($results);
        for($i=0; $i<$max; $i++){
            $z = $results[$i];
            if(sizeof($z) > 0){
                Session::flash('notAvailableTask', 'Overlap - One or more users are already scheduled for a task on these dates, Please change dates or users');
                return back();
            }
        }

        // update task
        $task->update($request->all());  
        $task->users()->attach($userId); 
        return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Gate::authorize('is-manager');
        // $task = Task::findOrFail($id);
        // $task->delete();
        // return redirect('tasks');
    }
}
