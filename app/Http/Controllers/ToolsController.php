<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\User;
use App\Toolstatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class ToolsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function availableTools()
    {        
        $tools=Tool::select('*')->where([['status_id',1],])->paginate('5');
        $tools->status_id = 1;
        $users = User::all();
        $statuses = Toolstatus::all();        
        return view('tools.index', compact('tools','users','statuses')); 
    }

    public function myTools()
    {        
        $tools = Tool::join('users','users.id','=','tools.user_id')->where([['user_id',Auth::id()],])->paginate('5');;
        $users = User::all();
        $statuses = Toolstatus::all();        
        return view('tools.index', compact('tools','users','statuses')); 
    }

    public function changeUser($tl_id, $uid = null){
        $tool = Tool::findOrFail($tl_id);
        $tool->user_id = $uid;
        $tool->status_id = 2;
        $tool->save(); 
        return back();

    }


    public function changeStatus($tl_id, $sid)
    {
        $tool = Tool::findOrFail($tl_id);    
        if($tool->status_id==$sid) return back();
        $tool->status_id = $sid;
        $tool->user_id = NULL;
        $tool->save();
        return back();
    } 


    public function index()
    {
        $tools=Tool::paginate(5);
        $users=User::all();
        $statuses=Toolstatus::all();
        return view('tools.index', compact('tools','users','statuses')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('is-manager');
        return view('tools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->name == NULL || $request->notes == NULL){
            Session::flash('NotFullForm', 'Please fill out the entire form, all fields are mandatory');
            return back();
        }
        $tool = new Tool();
        $tool=$tool->create($request->all());
        $tool->save();
        return redirect('tools');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('is-manager');
        $tool = Tool::findOrFail($id);
        $statuses = Toolstatus::all();
        return view('tools.edit', compact('tool','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->name == NULL || $request->notes == NULL){
            Session::flash('NotFullForm', 'Please fill out the entire form, all fields are mandatory');
            return back();
        }
        $tool = Tool::findOrFail($id);
        $tool->update($request->all());     
        return redirect('tools');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('is-manager');
        $tool = Tool::findOrFail($id);
        $tool->delete();
        return redirect('tools');
    }
}
