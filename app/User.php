<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

       /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tasks()
    {
        return $this->belongsToMany(Task::class,'task_user','user_id','task_id');
    }

    public function tools()
    {
        return $this->hasMany('App\Tool');
    } 

    public function roles(){
        return $this->belongsToMany(Role::class,'user_role','user_id','role_id');
    }

    
    public static function employee($task_id){
        $employee = DB::table('task_user')->where('task_id',$task_id)->pluck('user_id');
        return self::find($employee)->all();
        
    }

    public function isManager(){
        $roles = $this->roles;
        if(!isset($roles)) return false;
        foreach($roles as $role){
            if($role->name === 'manager') return true; 
        } 
        return false; 
    }


}
