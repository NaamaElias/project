<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class Task extends Model
{
    protected $fillable = [
        'task_description', 'start_date', 'estimated_end_date', 'status_id'
    ];

        public function users(){
            return $this->belongsToMany(User::class,'task_user','task_id','user_id');
        }

        public function status(){
            return $this->belongsTo('App\Status','status_id');
        }

   


     


       
       

}


