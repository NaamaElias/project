@extends('layouts.app')



@section('content')
@include('layouts.headers.list')

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Create Task') }}</h3>
                        </div>
                    </div>
                </div>
@if(Session::has('NotFullForm'))
<div class = 'alert alert-danger'>
    {{Session::get('NotFullForm')}}
</div>
@endif
@if(Session::has('wrongDates'))
<div class = 'alert alert-danger'>
    {{Session::get('wrongDates')}}
</div>
@endif
@if(Session::has('notAvailableTask'))
<div class = 'alert alert-danger'>
    {{Session::get('notAvailableTask')}}
</div>
@endif

<div class="container"> 
    <div class="col"> 
        <form method = "post" action = "{{action('TasksController@store')}}">
        @csrf 
        <div class="form-group">

        
            <label for = "task_description">Task Description</label>
            <input type = "text" class="form-control" name = "task_description">
        </div>     
        <div class="form-group">
            <label for = "start_date">Start Date</label>
            <input type = "date" class="form-control" name = "start_date">
        </div>
        <div class="form-group">
            <label for = "estimated_end_date">Estimated End Date</label>
            <input type = "date" class="form-control" name = "estimated_end_date">
        </div> 
        <div class="form-group">
            <label for = "employee">Choose Employee</label>
            @foreach($users as $user)
            <input type="checkbox" name="user_id[]" value={{$user->id}}>
            {{$user->name}}
            @endforeach
        </div> 
        <div>
            <input type = "submit" name = "submit" class="btn btn-sm btn-default"  value = "Create Task">
        </div>                       
        </form>  
    </div>
</div>
@endsection