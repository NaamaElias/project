@extends('layouts.app')

@section('content')

@include('layouts.headers.list')

        <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Tasks') }}</h3>
                            </div>
                            @if(Gate::allows('is-manager'))

                            <div class="col-4 text-right">
                            <form method="POST" action="{{route('tasks.deleteall')}}">
                                @csrf
                                @METHOD('DELETE')
                                
                                <input type="submit" class="btn btn-sm btn-danger" value="Delete All" >

                                <a href="{{url('/tasks/create')}}" class="btn btn-sm btn-primary">{{ __('Add New Task') }}</a>
                            </div>
                            @endif 
                        </div>
                    </div>
                    @if(Session::has('notallowed'))
                    <div class = 'alert alert-danger'>
                        {{Session::get('notallowed')}}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __(' ') }}</th>
                                    <th scope="col">{{ __('Id') }}</th>
                                    <th scope="col">{{ __('Description') }}</th>
                                    <th scope="col">{{ __('Start Date') }}</th>
                                    <th scope="col">{{ __('End Date') }}</th>
                                    <th scope="col">{{ __('Employees') }}</th>
                                    <th scope="col">{{ __('Status') }}</th>
                                    <th scope="col">{{ __('Created') }}</th>
                                    <th scope="col">{{ __('Updated') }}</th>
                                </tr>
                            </thead>      

                            <tbody>
                            @foreach($tasks as $task)
                                <tr>
                                <td><input type="checkbox" name="id[]" value={{$task->id}}></td>
                                <td>{{$task->id}}</td>
                                <td>{{$task->task_description}}</td>
                                <td>{{$task->start_date}}</td>
                                <td>{{$task->estimated_end_date}}</td>
                                <td>
                                    @foreach(App\User::employee($task->id) as $user)
                                        {{$user->name}} <br>
                                    @endforeach
                                </td>
                                <td>
                                <div class="dropdown">
                                @if (null != App\Status::switch($task->status_id))    
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(isset($task->status_id))
                                    {{$task->status->name ?? 'bla'}}
                                    @endif
                                </button>
                                @else 
                                {{$task->status->name}}
                                @endif
                                                            
                                @if (App\Status::switch($task->status_id) != null )
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach(App\Status::switch($task->status_id) as $status)
                                    <a class="dropdown-item" href="{{route('tasks.changestatus', [$task->id,$status->id])}}">{{$status->name}}</a>
                                    @endforeach                               
                                </div>
                                @endif
                                </div>   
                                </td>
                                <td>{{$task->created_at}}</td>
                                <td>{{$task->updated_at}}</td>

                                   
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                   

                                                            
                                                            <a class="dropdown-item" href="{{route('tasks.edit',$task->id)}}">{{ __('Edit') }}</a>
    
                                                    
                                                       
                                                </div>
                                            </div>
                                        </td>
                                       
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $tasks->links() }}
                        </form>    
                    </div>
                    
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
 
