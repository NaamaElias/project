@extends('layouts.app')


@section('content')
@include('layouts.headers.list')
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Edit Tool') }}</h3>
                        </div>
                    </div>
                </div>
@if(Session::has('NotFullForm'))
<div class = 'alert alert-danger'>
    {{Session::get('NotFullForm')}}
</div>
@endif
<div class="container"> 
    <div class="col"> 
        <form method = "post" action = "{{action('ToolsController@update', $tool->id)}}">
        @csrf
        @METHOD('PATCH')
             <div class="form-group">
            <label for = "name"><b>Name</b></label>
            <input type = "text" class="form-control" name = "name" value = {{$tool->name}}>
        </div>     
        <div class="form-group">
            <label for = "start_date"><b>Notes</b></label>
            <input type = "text" class="form-control" name = "notes" value = {{$tool->notes}}>
        </div>
        <div>
            <input type = "submit" name = "submit" class="btn btn-sm btn-default" value = "Update Tool">
        </div>

        </form> 
    </div>
</div> 
@endsection





