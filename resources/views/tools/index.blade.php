@extends('layouts.app')

@section('content')
@include('layouts.headers.list')

    <div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0"><a href="{{ route('tools.index') }}" font-color:black>{{ __('Tools') }}</a></h3>
                        </div> 
                        <div class="col-4 text-right">
                        @if(Gate::allows('is-manager'))   
                            <a href="{{url('/tools/create')}}" class="btn btn-sm btn-primary">{{ __('Add New Tool') }}</a>
                        @endif  
                        </div>
                        
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('Id') }}</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Notes') }}</th>
                                <th scope="col">{{ __('Employees') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                                <th scope="col">{{ __('Created') }}</th>
                                <th scope="col">{{ __('Updated') }}</th>
                            </tr>
                        </thead>      

                        <tbody>
                        @foreach($tools as $tool)
                                <tr>
                                <td>{{$tool->id}}</td>
                                <td>{{$tool->name}}</td>
                                <td>{{$tool->notes}}</td>        
                                <td> 
                                @if($tool->status_id !=4) 
                                        
                                    @if(isset($tool->user_id))
                                      {{$tool->user->name}}  
                                    @else
                                    <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Assign User
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($users as $user)
                                    <a class="dropdown-item" href="{{route('tools.changeuser',[$tool->id,$user->id])}}">{{$user->name}}</a>
                                    @endforeach
                                    </div>
                                    </div>
                                    @endif 
                                @else
                                      Can't Assign Employee
                                @endif 
                                </td>
                                <td>          
                                <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(isset($tool->status_id))
                                      {{$tool->toolstatus->name}}  
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($statuses as $status)
                                    @if($status->id != $tool->status_id && $status->id != 2)
                                        <a class="dropdown-item" href="{{route('tools.changestatus',[$tool->id,$status->id])}}">{{$status->name}}</a>
                                    @endif
                                @endforeach
                                </div>
                              </div>  
                                </td>
                                <td>{{$tool->created_at}}</td>
                                <td>{{$tool->updated_at}}</td>

                                @if(Gate::allows('is-manager'))
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                      
                                          <form action="{{ route('tools.destroy', $tool) }}" method="post">
                                              @csrf
                                              @method('delete')
                                             
                                              <a class="dropdown-item" href="{{route('tools.edit',$tool->id)}}">{{ __('Edit') }}</a>
                                             
                                              <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this tool?") }}') ? this.parentElement.submit() : ''">
                                                  {{ __('Delete') }}
                                              </button>
                                              @endif
                                          </form>    
                                            
                                                
                                        </div>
                                    </div>
                                </td>
                                    
                              </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $tools->links() }}
                </div>
                
            </div>
        </div>
    </div>
        
    @include('layouts.footers.auth')
  </div>
@endsection
 

















