@extends('layouts.app')



@section('content')
@include('layouts.headers.list')
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Create Tool') }}</h3>
                        </div>
                    </div>
                </div>
@if(Session::has('NotFullForm'))
<div class = 'alert alert-danger'>
    {{Session::get('NotFullForm')}}
</div>
@endif

<div class="container"> 
    <div class="col"> 
        <form method = "post" action = "{{action('ToolsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Name</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group">
            <label for = "tool_description">Notes</label>
            <input type = "text" class="form-control" name = "notes">
        </div>

        <div>
            <input type = "submit" name = "submit" class="btn btn-sm btn-default" value = "Add Tool">
        </div>                       
        </form> 
    </div>
</div> 
@endsection