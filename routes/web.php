<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# Tasks Routes
Route::resource('tasks', 'TasksController')->middleware('auth');
Route::get('tasks/changestatus/{id}/{sid}', 'TasksController@changeStatus')->name('tasks.changestatus')->middleware('auth');

Route::get('tasks/delete/{id}', 'TasksController@destroy')->name('tasks.delete')->middleware('auth');

Route::get('tasks/changeuser/{tid}/{uid?}', 'TasksController@changeUser')->name('task.changeuser')->middleware('auth');

Route::get('mytasks', 'TasksController@myTasks')->name('tasks.mytasks')->middleware('auth');

Route::get('completedtasks', 'TasksController@completedTasks')->name('tasks.completedtasks')->middleware('auth');

Route::delete('deleteall', 'TasksController@deleteAll')->name('tasks.deleteall')->middleware('auth');


# Tools Routes
Route::resource('tools', 'ToolsController')->middleware('auth');

Route::get('tools/changeuser/{tl_id}/{uid?}', 'ToolsController@changeUser')->name('tools.changeuser')->middleware('auth');

Route::get('tools/changestatus/{tl_id}/{sid?}', 'ToolsController@changeStatus')->name('tools.changestatus')->middleware('auth');

Route::get('mytools', 'ToolsController@myTools')->name('tools.mytools')->middleware('auth');

Route::get('availabletools', 'ToolsController@availableTools')->name('tools.availabletools')->middleware('auth');

Route::get('tools/delete/{id}', 'ToolsController@destroy')->name('tools.delete')->middleware('auth');




# Laravel Routes
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

