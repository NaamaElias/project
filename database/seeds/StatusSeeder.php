<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'name' => 'accepted',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'canceled',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],        
            [
                'name' => 'in process',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],   
            [
                'name' => 'done',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            ]);                
           
    
    }
}
